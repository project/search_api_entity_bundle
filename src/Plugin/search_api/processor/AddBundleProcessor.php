<?php

namespace Drupal\search_api_entity_bundle\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 * Adds the item's bundles to the indexed data.
 *
 * @SearchApiProcessor(
 *   id = "add_bundle",
 *   label = @Translation("Entity Bundle"),
 *   description = @Translation("Adds the item's Bundle to the indexed data."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class AddBundleProcessor extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Entity Bundles'),
        'description' => $this->t('The entity bundles'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['search_bundles'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    // Adds the entity bundle to filter by.
    $itemObject  = $item->getOriginalObject();
    $datasource  = $item->getDatasource();
    $bundle = $datasource->getItemBundle($itemObject);
    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'search_bundles');
    foreach ($fields as $field) {
      $field->addValue(ucwords($bundle)); 
    }
  }
}
