# Search API Entity Bundle.

This module adds a Search API Processor to add the option to index the entity bundles. 
This is especially useful when you want to have a facet filter by bundles, for examples Articles, Pages, Users, etc.

# Installation.
Install this module as you normally install Drupal 8/9 modules.
To add the entity bundles go to the index fields configuration page and add the "Entity Bundles" field, it must be on the General section.
